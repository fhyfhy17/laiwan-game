package com.java80.littlegame.service.gateway.websocket;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.java80.littlegame.common.msg.BaseMsg;

public class MsgQueueHelper {
	private static ConcurrentLinkedQueue<BaseMsg> msgQueue = new ConcurrentLinkedQueue<>();
	private static MsgQueueHelper ins = new MsgQueueHelper();
	private Thread sendThread = new Thread(new Runnable() {
		@Override
		public void run() {
			while (true) {
				for (int i = 0; i < 100; i++) {
					BaseMsg msg = msgQueue.poll();
					if (msg != null) {
						sendMsgToClient(msg);
					}
				}
				Thread.yield();
			}
		}
	});

	private MsgQueueHelper() {
		sendThread.start();
	}

	public static MsgQueueHelper getInstance() {
		return ins;
	}

	public void addMsg(BaseMsg msg) {
		msgQueue.offer(msg);
	}

	private void sendMsgToClient(BaseMsg msg) {
		MessageHelper.outMsgToClient(msg);
	}
}
