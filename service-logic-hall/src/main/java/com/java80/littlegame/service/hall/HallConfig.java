package com.java80.littlegame.service.hall;

import java.util.Properties;

import com.java80.littlegame.common.util.LoadPropertiesFileUtil;

public class HallConfig {
	private static String queueName;
	private static String serviceId;
	private static String instanceName;
	static {
		init();
	}

	public static void init() {
		Properties p = LoadPropertiesFileUtil.loadProperties("../config/cfg.properties");
		queueName = p.getProperty("service.hall.self.queuename");
		serviceId = p.getProperty("service.hall.id");
		instanceName = p.getProperty("service.hall.insname");
	}

	public static String getQueueName() {
		return queueName;
	}

	public static String getServiceId() {
		return serviceId;
	}

	public static String getInstanceName() {
		return instanceName;
	}

}
