package com.java80.littlegame.service.hall.handler;

import com.java80.littlegame.common.base.work.BaseHandler;
import com.java80.littlegame.common.base.work.Task;
import com.java80.littlegame.common.base.work.ThreadPools;
import com.java80.littlegame.common.msg.BaseMsg;
import com.java80.littlegame.common.msg.ProtoHelper;
import com.java80.littlegame.common.msg.ProtoList;
import com.java80.littlegame.common.msg.cluster.RefreshGameUserMessage;
import com.java80.littlegame.common.msg.hall.PullAndPushGameSystemMessage;
import com.java80.littlegame.common.msg.hall.UserLoginMessage;
import com.java80.littlegame.common.msg.hall.UserRegisterMessage;
import com.java80.littlegame.service.hall.handler.subhandler.GameHandler;
import com.java80.littlegame.service.hall.handler.subhandler.GameUserHandler;

public class HallHandler extends BaseHandler {
	private static HallHandler ins = new HallHandler();

	private HallHandler() {
	}

	public static HallHandler getInstance() {
		return ins;
	}

	private GameUserHandler gameUserHdl = new GameUserHandler();
	private GameHandler gameHdl = new GameHandler();

	public void onMessage(BaseMsg msg) {
		switch (msg.getCode()) {
		case ProtoList.MSG_CODE_LOGIN:
			gameUserHdl.doLogin((UserLoginMessage) msg);
			break;
		case ProtoList.MSG_CODE_REGISTER:
			gameUserHdl.doRegister((UserRegisterMessage) msg);
			break;
		case ProtoList.MSG_CODE_PULLANDPUSHGAMESYSTEM:
			gameHdl.pullAndpushgamesystem((PullAndPushGameSystemMessage) msg);
			break;
		case ProtoList.MSG_CODE_REFRESH_GAMEUSER:
			gameUserHdl.refreshGameUser((RefreshGameUserMessage) msg);
			break;
		default:
			break;
		}
	}

	@Override
	public Object onReceive(String obj) {
		// 收到消息 丢给actor处理
		BaseMsg baseMsg = ProtoHelper.parseJSON(obj.toString());
		switch (baseMsg.getType()) {
		case ProtoList.MSG_TYPE_CLUSTER:
			// 集群消息有可能没有发送者
			break;
		case ProtoList.MSG_TYPE_TIMER:
			// 定时调度消息有可能没有发送者
			break;
		default:
			// long senderUserId = baseMsg.getSenderUserId();

			break;
		}
		// TODO 先用线程池操作着，下次再改了*/
		ThreadPools.addTask(new Task() {
			@Override
			public void work() {
				onMessage(baseMsg);
			}
		});
		return null;
	}

}
