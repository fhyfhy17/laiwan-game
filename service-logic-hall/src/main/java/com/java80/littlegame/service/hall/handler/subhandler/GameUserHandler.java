package com.java80.littlegame.service.hall.handler.subhandler;

import java.util.Date;

import com.java80.littlegame.common.base.data.GameUser;
import com.java80.littlegame.common.base.data.GameUserMgr;
import com.java80.littlegame.common.base.vertx.ServiceStatusHelper;
import com.java80.littlegame.common.base.vertx.VertxMessageHelper;
import com.java80.littlegame.common.consts.SystemConsts;
import com.java80.littlegame.common.msg.cluster.RefreshGameUserMessage;
import com.java80.littlegame.common.msg.hall.UserLoginMessage;
import com.java80.littlegame.common.msg.hall.UserRegisterMessage;
import com.java80.littlegame.common.orm.model.UUserInfo;
import com.java80.littlegame.common.service.user.UserService;
import com.java80.littlegame.common.util.MD5Utils;
import com.java80.littlegame.service.hall.HallConfig;

public class GameUserHandler {

	public void refreshGameUser(RefreshGameUserMessage msg) {
		if (!HallConfig.getServiceId().equals(msg.getSourceServiceId())) {
			GameUserMgr.refreshGameUser(msg.getUserId());
		}
	}

	public void doRegister(UserRegisterMessage msg) {
		// 检查登录名
		String loginName = msg.getLoginName();
		UserService userService = UserService.getInstance();
		if (userService.findByLoginName(loginName) == null) {
			msg.setResult(1);
			String nickName = msg.getNickName();
			String password = msg.getPassword();
			try {
				String pwd = MD5Utils.md5hex32(password);
				UUserInfo u = new UUserInfo();
				u.setCreateTime(new Date());
				u.setLoginName(loginName);
				u.setNickName(nickName);
				u.setPassword(pwd);
				userService.register(u);
			} catch (Exception e) {
				e.printStackTrace();
				msg.setResult(-2);
			}
		} else {
			msg.setResult(-1);
		}

		VertxMessageHelper.broadcastMessageToService(SystemConsts.SERVICE_TYPE_GATEWAY, msg);
	}

	public void doLogin(UserLoginMessage msg) {
		String password = msg.getPassword();
		String userName = msg.getLoginName();
		try {
			password = MD5Utils.md5hex32(password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		UUserInfo userInfo = UserService.getInstance().login(userName, password);
		if (userInfo != null) {
			msg.setResult(1);
			msg.setRecUserId(userInfo.getId());
			// 查找可用service,随机分配room节点和game节点

			GameUser gu = new GameUser();
			gu.setHallServiceId(HallConfig.getServiceId());
			gu.setUserId(userInfo.getId());
			gu.setGameServiceId(ServiceStatusHelper.randServiceStatus(SystemConsts.SERVICE_TYPE_GAME).getServiceId());
			GameUserMgr.addGameUser(gu, () -> {
				// 发送刷新通知
				RefreshGameUserMessage refreshGameUserMessage = new RefreshGameUserMessage();
				refreshGameUserMessage.setUserId(gu.getUserId());
				refreshGameUserMessage.setSourceServiceId(HallConfig.getServiceId());
				VertxMessageHelper.broadcastMessageToAllService(refreshGameUserMessage);
			});
		} else {
			msg.setResult(0);

		}
		VertxMessageHelper.broadcastMessageToService(SystemConsts.SERVICE_TYPE_GATEWAY, msg);
	}
}
