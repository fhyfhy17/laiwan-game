package com.java80.littlegame.common.consts;

public class SystemConsts {
	public static final int SERVICE_TYPE_GATEWAY = 1;
	public static final int SERVICE_TYPE_HALL = 2;
	public static final int SERVICE_TYPE_QUARTZ = 3;
	public static final int SERVICE_TYPE_GAME = 4;
}
