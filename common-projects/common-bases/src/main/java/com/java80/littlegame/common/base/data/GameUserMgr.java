package com.java80.littlegame.common.base.data;

import java.util.concurrent.ConcurrentHashMap;

import com.java80.littlegame.common.cache.CacheObj;
import com.java80.littlegame.common.cache.lv2.ObjectCacheRedisService;
import com.java80.littlegame.common.consts.CachePrefixConsts;

/**
 * 相当于本地缓存 涉及到 缓存同步问题<br>
 * 暂定策略：本地缓存一份，redis一份，无论哪个节点发生gameuser对象改变，都发送广播通知其他节点去redis重新读一份
 * 修改和删除的时候需要发送广播通知
 * 
 * @author lenovo
 *
 */
public class GameUserMgr {
	private static ConcurrentHashMap<Long, GameUser> gameUsers = new ConcurrentHashMap<>();

	public static void refreshGameUser(long userId) {
		GameUser gameUser = ObjectCacheRedisService.get(CacheObj.makeKeyName(CachePrefixConsts.CACHE_KEY_GAMEUSER + userId),
				GameUser.class);
		gameUsers.put(Long.valueOf(gameUser.getUserId()), gameUser);
	}

	public static GameUser getGameUser(long userId) {
		GameUser gameUser = gameUsers.get(Long.valueOf(userId));
		if (gameUser == null) {
			// 去redis读一份 放本地
			gameUser = ObjectCacheRedisService.get(CacheObj.makeKeyName(CachePrefixConsts.CACHE_KEY_GAMEUSER + userId),
					GameUser.class);
			gameUsers.put(Long.valueOf(gameUser.getUserId()), gameUser);
		}
		return gameUser;
	}

	public static void removeGameUser(long userId, RefreshGameUserHandler handler) {
		if (handler != null) {
			handler.refreshGameUser();
		}
		gameUsers.remove(Long.valueOf(userId));
	}

	public static void addGameUser(GameUser gameUser, RefreshGameUserHandler handler) {
		ObjectCacheRedisService.saveObject(CacheObj.makeKeyName(CachePrefixConsts.CACHE_KEY_GAMEUSER + gameUser.getUserId()),
				gameUser);
		gameUsers.put(Long.valueOf(gameUser.getUserId()), gameUser);
		if (handler != null) {
			handler.refreshGameUser();
		}
	}
}
