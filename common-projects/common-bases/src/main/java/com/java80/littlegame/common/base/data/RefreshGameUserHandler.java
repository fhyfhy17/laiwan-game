package com.java80.littlegame.common.base.data;

@FunctionalInterface
public interface RefreshGameUserHandler {
	void refreshGameUser();
}
