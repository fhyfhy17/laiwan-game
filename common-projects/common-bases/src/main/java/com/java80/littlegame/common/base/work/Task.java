package com.java80.littlegame.common.base.work;

public abstract class Task {
	public abstract void work();
}
