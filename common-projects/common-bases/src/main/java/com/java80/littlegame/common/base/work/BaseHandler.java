package com.java80.littlegame.common.base.work;

/** handler应当是单例模式 */
public abstract class BaseHandler {
	/**
	 * obj 是json
	 * 
	 * @param obj
	 * @return
	 */

	public abstract Object onReceive(String obj);
}
