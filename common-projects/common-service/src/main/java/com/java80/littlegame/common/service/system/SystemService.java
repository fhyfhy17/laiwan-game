package com.java80.littlegame.common.service.system;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.java80.littlegame.common.orm.mapper.SGameConfigMapper;
import com.java80.littlegame.common.orm.mapper.SGameInfoMapper;
import com.java80.littlegame.common.orm.model.SGameConfig;
import com.java80.littlegame.common.orm.model.SGameInfo;
import com.java80.littlegame.common.orm.utils.SqlSessionUtils;

public class SystemService {
	private static SystemService ins=new SystemService();
	private SystemService(){}
	public static SystemService getInstance(){
		return ins;
	}
	public List<SGameInfo> findAllGameInfo(){
		SqlSession session = null;
		try {
			session = SqlSessionUtils.getSqlSession(true);
			SGameInfoMapper mapper = session.getMapper(SGameInfoMapper.class);
			List<SGameInfo> list = mapper.findAll(new SGameInfo());
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SqlSessionUtils.closeSqlSession(session);
		}
		return null;
	}
	public List<SGameConfig> findAllGameConfig(){
		SqlSession session = null;
		try {
			session = SqlSessionUtils.getSqlSession(true);
			SGameConfigMapper mapper = session.getMapper(SGameConfigMapper.class);
			List<SGameConfig> list = mapper.findAll(new SGameConfig());
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SqlSessionUtils.closeSqlSession(session);
		}
		return null;
	}
}
