package com.java80.littlegame.common.service.room;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.java80.littlegame.common.orm.mapper.URoomInfoMapper;
import com.java80.littlegame.common.orm.mapper.UUserRoomInfoMapper;
import com.java80.littlegame.common.orm.model.URoomInfo;
import com.java80.littlegame.common.orm.model.UUserRoomInfo;
import com.java80.littlegame.common.orm.utils.SqlSessionUtils;

public class RoomService {
	private RoomService(){}
	private static RoomService ins=new RoomService();
	public static RoomService getInstance(){
		return ins;
	}
	public void deleteRoom(int roomId){
		SqlSession session = null;
		try {
			session = SqlSessionUtils.getSqlSession(false);
			URoomInfoMapper roomInfoMapper = session.getMapper(URoomInfoMapper.class);
			URoomInfo r=new URoomInfo();
			r.setRoomId(roomId);
			roomInfoMapper.deleteAccuracy(r);
			UUserRoomInfoMapper mapper = session.getMapper(UUserRoomInfoMapper.class);
			UUserRoomInfo d=new UUserRoomInfo();
			d.setRoomId(roomId);
			mapper.deleteAccuracy(d);
			session.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SqlSessionUtils.closeSqlSession(session);
		}
	}
	public void deleteByUserId(long userId){
		SqlSession session = null;
		try {
			session = SqlSessionUtils.getSqlSession(true);
			URoomInfoMapper mapper = session.getMapper(URoomInfoMapper.class);
			URoomInfo d=new URoomInfo();
			d.setUserId(userId);
			mapper.deleteAccuracy(d);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SqlSessionUtils.closeSqlSession(session);
		}
	}
	public List<UUserRoomInfo> findUserRoomInfoByRoom(int roomId){
		SqlSession session = null;
		try {
			session = SqlSessionUtils.getSqlSession(true);
			UUserRoomInfoMapper mapper = session.getMapper(UUserRoomInfoMapper.class);
			UUserRoomInfo d=new UUserRoomInfo();
			d.setRoomId(roomId);
			return mapper.findAll(d);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SqlSessionUtils.closeSqlSession(session);
		}
		return null;
	}
}
