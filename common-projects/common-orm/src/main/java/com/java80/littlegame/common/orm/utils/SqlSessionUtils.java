package com.java80.littlegame.common.orm.utils;

import java.io.InputStream;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class SqlSessionUtils {
	private static SqlSessionFactory factory=getFactory();
	private synchronized static SqlSessionFactory getFactory()  {
		String resource = "mybatis.xml";
		// 加载mybatis 的配置文件（它也加载关联的映射文件）
		InputStream is = SqlSessionUtils.class.getClassLoader().getResourceAsStream(resource);
		// 构建sqlSession 的工厂
		SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
		// MybatisSqlSessionFactoryBean sqlSessionFactoryBean =
		// test.sqlSessionFactoryBean();
		return factory;
	}
	/**session带事务操作的时候 需要手动commit*/
	public static SqlSession getSqlSession(boolean autoCommit) {
		SqlSession session = factory.openSession(autoCommit);
		return session;
	}
	public static void closeSqlSession(SqlSession session) {
		if(session!=null){
			session.close();
		}
	}
	
}
