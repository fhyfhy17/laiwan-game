package com.java80.littlegame.common.msg.cluster;

import com.java80.littlegame.common.msg.BaseMsg;
import com.java80.littlegame.common.msg.ProtoList;

public abstract class ClusterMessage extends BaseMsg {

	@Override
	public int getType() {
		return ProtoList.MSG_TYPE_CLUSTER;
	}
}
