package com.java80.littlegame.common.msg.hall;

import java.util.ArrayList;
import java.util.List;

import com.java80.littlegame.common.msg.BaseMsg;
import com.java80.littlegame.common.msg.ProtoList;

/**
 * 客户端请求推送消息 server<->client
 * 
 * @author
 *
 */
public class PullAndPushGameSystemMessage extends BaseMsg {
	private List<GameInfo> gameInfos = new ArrayList<>();

	public List<GameInfo> getGameInfos() {
		return gameInfos;
	}

	public void setGameInfos(List<GameInfo> gameInfos) {
		this.gameInfos = gameInfos;
	}

	@Override
	public int getType() {
		return ProtoList.MSG_TYPE_HALL;
	}

	@Override
	public int getCode() {
		return ProtoList.MSG_CODE_PULLANDPUSHGAMESYSTEM;
	}
	public static class GameInfo{
		private Integer id;
	    private String gameName;
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getGameName() {
			return gameName;
		}
		public void setGameName(String gameName) {
			this.gameName = gameName;
		}
 
	}
}
