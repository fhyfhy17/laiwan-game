package com.java80.littlegame.common.msg.game;

import com.java80.littlegame.common.msg.BaseMsg;
import com.java80.littlegame.common.msg.ProtoList;

/**
 * 创建房间 server<->client
 * 
 * @author
 *
 */
public class CreateRoomMessage extends BaseMsg {
	private int roomId;
	private int status;
	private int password;
	private long roomMaster;// 房主
	private int result;
	private int gameId;

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public long getRoomMaster() {
		return roomMaster;
	}

	public void setRoomMaster(long roomMaster) {
		this.roomMaster = roomMaster;
	}

	public int getPassword() {
		return password;
	}

	public void setPassword(int password) {
		this.password = password;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	@Override
	public int getType() {
		return ProtoList.MSG_TYPE_GAME;
	}

	@Override
	public int getCode() {
		return ProtoList.MSG_CODE_CREATE_ROOM;
	}

}
