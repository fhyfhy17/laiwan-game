package com.java80.littlegame.common.msg.cluster;

import com.java80.littlegame.common.msg.ProtoList;

public class DeskEndMessage extends ClusterMessage {
	private int roomId;

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	@Override
	public int getCode() {
		return ProtoList.MSG_CODE_DESKEND;
	}

}
