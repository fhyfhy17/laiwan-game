package com.java80.littlegame.common.msg.game;

import java.util.ArrayList;
import java.util.List;

import com.java80.littlegame.common.msg.BaseMsg;
import com.java80.littlegame.common.msg.ProtoList;

/**
 * 服务器广播给房内玩家有人加入
 *
 */
public class UserJoinRoomMessage extends BaseMsg {
	private long joinUserId;
	private List<PlayerInfo> players = new ArrayList<>();

	public long getJoinUserId() {
		return joinUserId;
	}

	public void setJoinUserId(long joinUserId) {
		this.joinUserId = joinUserId;
	}

	public List<PlayerInfo> getPlayers() {
		return players;
	}

	public void setPlayers(List<PlayerInfo> players) {
		this.players = players;
	}

	public static class PlayerInfo {
		private long id;
		private String nickName;

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getNickName() {
			return nickName;
		}

		public void setNickName(String nickName) {
			this.nickName = nickName;
		}
	}

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return ProtoList.MSG_TYPE_GAME;
	}

	@Override
	public int getCode() {
		// TODO Auto-generated method stub
		return ProtoList.MSG_CODE_USERJOINROOM;
	}
}
