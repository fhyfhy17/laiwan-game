package com.java80.littlegame.service.quartz;

import com.java80.littlegame.common.base.work.BaseHandler;
import com.java80.littlegame.common.base.work.Task;
import com.java80.littlegame.common.base.work.ThreadPools;
import com.java80.littlegame.common.msg.BaseMsg;
import com.java80.littlegame.common.msg.ProtoHelper;
import com.java80.littlegame.common.msg.timer.TimerMessage;

public class QuartzHandler extends BaseHandler {
	private static QuartzHandler ins = new QuartzHandler();

	public static QuartzHandler getInstance() {
		return ins;
	}

	private QuartzHandler() {
	}

	public void onMessage(BaseMsg msg) {
		if (msg instanceof TimerMessage) {
			CronTask ct = new CronTask(((TimerMessage) msg).getDelay(), msg.toString(),
					((TimerMessage) msg).getTargetQueue());
			CronJobTools.addSystemTask(ct);
		}
	}

	@Override
	public Object onReceive(String obj) {
		ThreadPools.addTask(new Task() {
			@Override
			public void work() {
				onMessage(ProtoHelper.parseJSON(obj.toString()));

			}
		});
		return null;
	}

}
