package com.java80.littlegame.service.game.handler.subhandler;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import com.java80.littlegame.common.base.data.GameUserMgr;
import com.java80.littlegame.common.base.data.SystemDataMgr;
import com.java80.littlegame.common.base.vertx.ServiceStatus;
import com.java80.littlegame.common.base.vertx.ServiceStatusHelper;
import com.java80.littlegame.common.base.vertx.VertxMessageHelper;
import com.java80.littlegame.common.consts.SystemConsts;
import com.java80.littlegame.common.msg.cluster.DeskEndMessage;
import com.java80.littlegame.common.msg.cluster.GameStartMessage;
import com.java80.littlegame.common.msg.game.DesktopEndMessage;
import com.java80.littlegame.common.msg.timer.DesktopExpridedMessage;
import com.java80.littlegame.common.orm.model.SGameConfig;
import com.java80.littlegame.common.orm.model.UUserRoomInfo;
import com.java80.littlegame.common.service.room.RoomService;
import com.java80.littlegame.service.game.GameConfig;
import com.java80.littlegame.service.game.desk.DeskSetting;
import com.java80.littlegame.service.game.desk.Desktop;
import com.java80.littlegame.service.game.desk.DesktopMgr;

public class DesktopHandler {
	public void deskend(DeskEndMessage msg, int reason) {
		Desktop desktop = DesktopMgr.removeDesktop(msg.getRoomId());

		RoomService.getInstance().deleteRoom(msg.getRoomId());
		// 发送消息
		List<Long> playerIds = desktop.getSetting().getPlayerIds();
		for (long id : playerIds) {
			DesktopEndMessage dem = new DesktopEndMessage();
			dem.setRoomId(msg.getRoomId());
			dem.setReason(reason);
			dem.setRecUserId(id);
			VertxMessageHelper.sendMessageToGateWay(GameUserMgr.getGameUser(id), dem);
		}
		desktop = null;
	}

	public void gameStart(GameStartMessage msg) {
		int gameId = msg.getGameId();
		int roomId = msg.getRoomId();
		DeskSetting set = new DeskSetting();
		set.setGameId(gameId);
		set.setRoomId(roomId);
		List<UUserRoomInfo> findByRoomId = RoomService.getInstance().findUserRoomInfoByRoom(roomId);
		List<Long> ids = new ArrayList<>();
		for (UUserRoomInfo uri : findByRoomId) {
			ids.add(uri.getUserId());
		}
		set.setPlayerIds(ids);
		TreeMap<Integer, SGameConfig> gameConfigs = SystemDataMgr.getGameConfigs();
		Desktop desk = null;
		try {
			Class<?> c = Class.forName(gameConfigs.get(gameId).getDesktopClass());
			Constructor<?> con = c.getConstructor(DeskSetting.class);
			desk = (Desktop) con.newInstance(set);
			DesktopMgr.addDesktop(roomId, desk, ids);
			desk.onStart();
			DesktopExpridedMessage em = new DesktopExpridedMessage();
			em.setDelay(gameConfigs.get(gameId).getExpride() * 1000);
			em.setGameId(gameId);
			em.setRoomId(roomId);
			em.setTargetQueue(GameConfig.getQueueName());
			ServiceStatus s = ServiceStatusHelper.randServiceStatus(SystemConsts.SERVICE_TYPE_QUARTZ);
			VertxMessageHelper.sendMessageToService(s.getServiceQueueName(), em.toString());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
